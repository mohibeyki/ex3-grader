passed = '✓'
failed = '✕'

# total 360 points
tests = {
    'checks that the API is up': 30,
    'tests that the user database can be reset': 30,

    'tests that user bbreau can be created': 1,
    'tests that user ssollers can be created': 1,
    'tests that user kkeels can be created': 1,
    'tests that user ttiggs can be created': 1,
    'tests that user mmontijo can be created': 1,
    'tests that user rrelf can be created': 1,
    'tests that user eeppinger can be created': 1,
    'tests that user rreeves can be created': 1,
    'tests that user ttownsley can be created': 1,
    'tests that user bbreeden can be created': 1,
    'tests that user ccavallo can be created': 1,
    'tests that user sstolz can be created': 1,
    'tests that user jjerkins can be created': 1,
    'tests that user jjolin can be created': 1,
    'tests that user llangner can be created': 1,
    'tests that user sshulman can be created': 1,
    'tests that user aasay can be created': 1,
    'tests that user jjun can be created': 1,
    'tests that user ggayhart can be created': 1,
    'tests that user xxiong can be created': 1,
    'tests that user ttsao can be created': 1,
    'tests that user llongtin can be created': 1,
    'tests that user mmayson can be created': 1,
    'tests that user ccasey can be created': 1,
    'tests that user ggoldfarb can be created': 1,
    'tests that user ddampier can be created': 1,
    'tests that user ddevoe can be created': 1,
    'tests that user dduquette can be created': 1,
    'tests that user aallensworth can be created': 1,
    'tests that user ddinan can be created': 1,

    'tests that attempts to create incomplete users are rejected': 30,

    'tests that attempts to create users w/o password are rejected': 30,

    'tests that existing users can\'t be created again with the same name': 30,

    'tests that user bbreau can be retrieved': 1,
    'tests that user ssollers can be retrieved': 1,
    'tests that user kkeels can be retrieved': 1,
    'tests that user ttiggs can be retrieved': 1,
    'tests that user mmontijo can be retrieved': 1,
    'tests that user rrelf can be retrieved': 1,
    'tests that user eeppinger can be retrieved': 1,
    'tests that user rreeves can be retrieved': 1,
    'tests that user ttownsley can be retrieved': 1,
    'tests that user bbreeden can be retrieved': 1,
    'tests that user ccavallo can be retrieved': 1,
    'tests that user sstolz can be retrieved': 1,
    'tests that user jjerkins can be retrieved': 1,
    'tests that user jjolin can be retrieved': 1,
    'tests that user llangner can be retrieved': 1,
    'tests that user sshulman can be retrieved': 1,
    'tests that user aasay can be retrieved': 1,
    'tests that user jjun can be retrieved': 1,
    'tests that user ggayhart can be retrieved': 1,
    'tests that user xxiong can be retrieved': 1,
    'tests that user ttsao can be retrieved': 1,
    'tests that user llongtin can be retrieved': 1,
    'tests that user mmayson can be retrieved': 1,
    'tests that user ccasey can be retrieved': 1,
    'tests that user ggoldfarb can be retrieved': 1,
    'tests that user ddampier can be retrieved': 1,
    'tests that user ddevoe can be retrieved': 1,
    'tests that user dduquette can be retrieved': 1,
    'tests that user aallensworth can be retrieved': 1,
    'tests that user ddinan can be retrieved': 1,

    # 2 times
    'tests that users can be listed': 30,

    'tests that a user can be deleted': 30,

    'tests that a user\'s fullname and password can be updated': 30,

    'checks that all passwords are encrypted properly': 30
}
